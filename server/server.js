/* eslint-env node */
import path from 'path';
import express from 'express';
import expressHandlebars from 'express-handlebars';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackConfig from './../webpack.config.js';

process.stdout.write("Starting sever...\n");

const app = express();

const compiler = webpack(webpackConfig);

if(process.env.NODE_ENV !== 'production') {
    app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: webpackConfig.output.publicPath }));
}

app.engine('hbs', expressHandlebars());
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, '/views'));

app.use('/coverage', express.static(path.join(__dirname, '/../public/coverage')));
app.use(express.static(path.join(__dirname, '/../public/build')));

app.get('*', (req, res) => {
    res.render('index', {
        production: process.env.NODE_ENV === 'production',
        appConfig: JSON.stringify({
            dataUrl: 'http://jsonplaceholder.typicode.com/users'
        })
    });
});

const port = 3000;
app.listen(port, function() {
    process.stdout.write("Server is running on port " + port + ".");
});