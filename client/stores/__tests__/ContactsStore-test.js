/* eslint-env node */
import sinon from 'sinon';
import expect from 'expect';
import requireUncached from './../../../utils/test/requireUncached';
import ContactsStore from './../ContactsStore';
import ContactsConstants from './../../constants/ContactsConstants';

describe('ContactsStore', function () {
    let AppDispatcher;
    let AppDispatcherMock;

    before(function () {
        AppDispatcher = ContactsStore.__get__('AppDispatcher');
        AppDispatcherMock = sinon.mock(AppDispatcher);
    });

    it('registers callback with dispatcher', function () {
        AppDispatcherMock
            .expects('register')
            .returns('dispatch_ID')
            .once();

        // Requiring again for this test, because AppDispatcher mock expectations need to be set before
        const ContactsStore = requireUncached(__dirname + '/../ContactsStore').default;

        expect(ContactsStore.dispatcherToken).toBe('dispatch_ID');

        AppDispatcherMock.verify();
    });

    describe('#(private)searchByName', function() {
        let searchByName;

        before(function() {
            searchByName = ContactsStore.__get__('searchByName');
        });

        it('return only contacts that contains searchText in name', function() {
            const contacts = [
                {
                    name: 'Maria'
                },
                {
                    name: 'Aria'
                },
                {
                    name: 'Joe'
                }
            ];

            const expectedResult = [
                {
                    name: 'Maria'
                },
                {
                    name: 'Aria'
                }
            ];

            const result = searchByName('ari', contacts);

            expect(result).toEqual(expectedResult);
        });

        it('return empty array if searchText is not found', function() {
            const contacts = [
                {
                    name: 'Maria'
                },
                {
                    name: 'Aria'
                },
                {
                    name: 'Joe'
                }
            ];

            const expectedResult = [];

            const result = searchByName('Millhouse', contacts);

            expect(result).toEqual(expectedResult);
        });

        it('return empty array if contacts are empty', function() {
            const contacts = [];

            const expectedResult = [];

            const result = searchByName('Millhouse', contacts);

            expect(result).toEqual(expectedResult);
        });
    });

    describe('#(private)searchContacts', function() {
        let searchContacts;
        let searchByNameStub;

        before(function() {
            searchContacts = ContactsStore.__get__('searchContacts');
        });

        beforeEach(function() {
            searchByNameStub = sinon.stub();
            ContactsStore.__set__('searchByName', searchByNameStub);
        });
        afterEach(function() {
            ContactsStore.__ResetDependency__('searchByName');
        });

        it('should return original contacts if searchText is undefined', function() {
            const contacts = 'original contacts';

            const result = searchContacts(undefined, contacts);

            expect(result).toBe(contacts);
            expect(searchByNameStub.called).toBe(false);
        });

        it('should return original contacts if searchText is null', function() {
            const contacts = 'original contacts';

            const result = searchContacts(null, contacts);

            expect(result).toBe(contacts);
            expect(searchByNameStub.called).toBe(false);
        });

        it('should return original contacts if searchText is empty string', function() {
            const contacts = 'original contacts';

            const result = searchContacts('', contacts);

            expect(result).toBe(contacts);
            expect(searchByNameStub.called).toBe(false);
        });

        it('should call searchByName and return its value', function() {
            const contacts = 'original contacts';

            searchByNameStub
                .returns('filtered contacts');

            const result = searchContacts('Bart', contacts);

            expect(result).toBe('filtered contacts');
            expect(searchByNameStub.withArgs('Bart', 'original contacts').calledOnce).toBe(true);
        });
    });

    describe('#(private)sortByName', function() {
        let sortByName;

        before(function() {
            sortByName = ContactsStore.__get__('sortByName');
        });

        it('should sort contacts ascending if order is undefined', function() {
            const contacts = [
                {
                    name: 'Maria'
                },
                {
                    name: 'Aria'
                },
                {
                    name: 'Joe'
                }
            ];

            const expectedResult = [
                {
                    name: 'Aria'
                },
                {
                    name: 'Joe'
                },
                {
                    name: 'Maria'
                }
            ];

            const result = sortByName(undefined, contacts);

            expect(result).toEqual(expectedResult);
        });

        it('should sort contacts ascending if order is SORT_ORDER_ASCENDING', function() {
            const contacts = [
                {
                    name: 'Maria'
                },
                {
                    name: 'Aria'
                },
                {
                    name: 'Joe'
                }
            ];

            const expectedResult = [
                {
                    name: 'Aria'
                },
                {
                    name: 'Joe'
                },
                {
                    name: 'Maria'
                }
            ];

            const result = sortByName(ContactsConstants.SORT_ORDER_ASCENDING, contacts);

            expect(result).toEqual(expectedResult);
        });

        it('should sort contacts descending if order is SORT_ORDER_DESCENDING', function() {
            const contacts = [
                {
                    name: 'Maria'
                },
                {
                    name: 'Aria'
                },
                {
                    name: 'Joe'
                }
            ];

            const expectedResult = [
                {
                    name: 'Maria'
                },
                {
                    name: 'Joe'
                },
                {
                    name: 'Aria'
                }
            ];

            const result = sortByName(ContactsConstants.SORT_ORDER_DESCENDING, contacts);

            expect(result).toEqual(expectedResult);
        });

        it('should return original array if it is already sorted', function() {
            const contacts = [
                {
                    name: 'Maria'
                },
                {
                    name: 'Joe'
                },
                {
                    name: 'Aria'
                }
            ];

            const expectedResult = [
                {
                    name: 'Maria'
                },
                {
                    name: 'Joe'
                },
                {
                    name: 'Aria'
                }
            ];

            const result = sortByName(ContactsConstants.SORT_ORDER_DESCENDING, contacts);

            expect(result).toEqual(expectedResult);
        });

        it('return empty array if contacts are empty', function() {
            const contacts = [];

            const expectedResult = [];

            const result = sortByName('Millhouse', contacts);

            expect(result).toEqual(expectedResult);
        });
    });

    describe('#(private)sortContacts', function() {
        let sortContacts;
        let sortByNameStub;

        before(function() {
            sortContacts = ContactsStore.__get__('sortContacts');
        });

        beforeEach(function() {
            sortByNameStub = sinon.stub();
            ContactsStore.__set__('sortByName', sortByNameStub);
        });
        afterEach(function() {
            ContactsStore.__ResetDependency__('sortByName');
        });

        it('should return original contacts if sortOrder is undefined', function() {
            const contacts = 'original contacts';

            const result = sortContacts(undefined, contacts);

            expect(result).toBe(contacts);
            expect(sortByNameStub.called).toBe(false);
        });

        it('should return original contacts if sortOrder is null', function() {
            const contacts = 'original contacts';

            const result = sortContacts(null, contacts);

            expect(result).toBe(contacts);
            expect(sortByNameStub.called).toBe(false);
        });


        it('should return original contacts if sortOrder is SORT_ORDER_DONT_SORT', function() {
            const contacts = 'original contacts';

            const result = sortContacts(ContactsConstants.SORT_ORDER_DONT_SORT, contacts);

            expect(result).toBe(contacts);
            expect(sortByNameStub.called).toBe(false);
        });

        it('should call searchByName and return its value', function() {
            const contacts = 'original contacts';

            sortByNameStub
                .returns('sorted contacts');

            const result = sortContacts(ContactsConstants.SORT_ORDER_DESCENDING, contacts);

            expect(result).toBe('sorted contacts');
            expect(sortByNameStub.withArgs(ContactsConstants.SORT_ORDER_DESCENDING, 'original contacts').calledOnce).toBe(true);
        });
    });

    describe('#(private)processContacts', function() {
        let processContacts;
        let searchContactsStub;
        let sortContactsStub;

        before(function() {
            processContacts = ContactsStore.__get__('processContacts');
        });

        beforeEach(function() {
            searchContactsStub = sinon.stub();
            ContactsStore.__set__('searchContacts', searchContactsStub);

            sortContactsStub = sinon.stub();
            ContactsStore.__set__('sortContacts', sortContactsStub);
        });
        afterEach(function() {
            ContactsStore.__ResetDependency__('searchContacts');
            ContactsStore.__ResetDependency__('sortContacts');
        });

        it('should call searchContacts,pass returned value to sortContacts and return value', function() {
            const searchText = 'searching for a heart of gold';
            const sortOrder = 'ascending';
            const contacts = 'original contacts';

            searchContactsStub
                .returns('filtered contacts');

            sortContactsStub
                .returns('sorted contacts');

            const result = processContacts(searchText, sortOrder, contacts);

            expect(result).toBe('sorted contacts');
            expect(searchContactsStub.withArgs(searchText, contacts).calledOnce).toBe(true, 'searchContacts was not called properly');
            expect(sortContactsStub.withArgs(sortOrder, 'filtered contacts').calledOnce).toBe(true, 'sortContacts was not called properly');
        });
    });

    describe('#getState', function() {
        it('should return _state', function() {
            ContactsStore.__set__('_state', 'some state');

            const result = ContactsStore.getState();

            expect(result).toBe('some state');

            ContactsStore.__ResetDependency__('_state');
        });
    });

    describe('#emitChange', function() {
        it('should call this.emit with change argument', function () {
            const ContactsStoreMock = sinon.mock(ContactsStore);

            ContactsStoreMock
                .expects('emit')
                .withArgs('change')
                .once();

            ContactsStore.emitChange();

            ContactsStoreMock.verify();
        });
    });

    describe('#addChangeListener', function() {
        it('registers change listener', function () {
            const ContactsStoreMock = sinon.mock(ContactsStore);

            const callback = function () {};

            ContactsStoreMock
                .expects('on')
                .withArgs('change', callback)
                .once();

            ContactsStore.addChangeListener(callback);

            ContactsStoreMock.verify();
        });
    });

    describe('#removeChangeListener', function() {
        it('removes change listener', function () {
            const ContactsStoreMock = sinon.mock(ContactsStore);

            const callback = function () {};

            ContactsStoreMock
                .expects('removeListener')
                .withArgs('change', callback)
                .once();

            ContactsStore.removeChangeListener(callback);

            ContactsStoreMock.verify();
        });
    });

    describe('#handleAction', function(){
        it('emits change event on action', function () {
            const action = {
                actionType: ContactsConstants.FETCH_CONTACT_LIST
            };

            const ContactsStoreMock = sinon.mock(ContactsStore);
            ContactsStoreMock
                .expects('emit')
                .withArgs('change')
                .once();


            ContactsStore.handleAction(action);

            ContactsStoreMock.verify();
        });

        it('should not emit change event on unknown action', function () {
            const action = {
                actionType: 'UNKNOWN_ACTION',
                data: {}
            };

            const ContactsStoreMock = sinon.mock(ContactsStore);
            ContactsStoreMock
                .expects('emit')
                .never();

            ContactsStore.handleAction(action);

            ContactsStoreMock.verify();
        });

        describe('FETCH_CONTACT_LIST', function() {
            it('should set status.loading to true', function () {
                const action = {
                    actionType: ContactsConstants.FETCH_CONTACT_LIST
                };

                ContactsStore.handleAction(action);
                const state = ContactsStore.__get__('_state');

                expect(state.status.loading).toBe(true);
                expect(state.status.error).toBe(false);
            });
        });

        describe('LOAD_CONTACT', function() {
            it('should set status.loading to true', function () {
                const action = {
                    actionType: ContactsConstants.FETCH_CONTACT_LIST
                };

                ContactsStore.handleAction(action);
                const state = ContactsStore.__get__('_state');

                expect(state.status.loading).toBe(true);
                expect(state.status.error).toBe(false);
            });
        });

        describe('FETCH_CONTACT_LIST_SUCCESS', function() {
            it('should set status.fetched true and call processContacts', function () {
                const action = {
                    actionType: ContactsConstants.FETCH_CONTACT_LIST_SUCCESS,
                    data: 'fetched contacts'
                };

                const processContactsStub = sinon.stub();
                ContactsStore.__set__('processContacts', processContactsStub);
                processContactsStub
                    .returns('processed contacts');

                ContactsStore.__set__('_state', ContactsStore.__get__('_emptyState'));

                ContactsStore.handleAction(action);
                const state = ContactsStore.__get__('_state');

                expect(processContactsStub.withArgs('', ContactsConstants.SORT_ORDER_DONT_SORT, 'fetched contacts'));
                expect(state.fetchedContacts).toBe('fetched contacts');
                expect(state.contacts).toBe('processed contacts');
                expect(state.status.fetched).toBe(true);
                expect(state.status.loading).toBe(false);
                expect(state.status.error).toBe(false);

                ContactsStore.__ResetDependency__('processContacts');
            });
        });

        describe('FETCH_CONTACT_LIST_ERROR', function() {
            it('should set status.error to true', function () {
                const action = {
                    actionType: ContactsConstants.FETCH_CONTACT_LIST_ERROR
                };

                ContactsStore.handleAction(action);
                const state = ContactsStore.__get__('_state');

                expect(state.status.loading).toBe(false);
                expect(state.status.error).toBe(true);
            });
        });

        describe('SEARCH', function() {
            it('should set searchText and call processContacts', function () {
                const action = {
                    actionType: ContactsConstants.SEARCH,
                    data: 'finding neverland'
                };

                const processContactsStub = sinon.stub();
                ContactsStore.__set__('processContacts', processContactsStub);
                processContactsStub
                    .returns('processed contacts');

                const initialState = Object.assign({}, ContactsStore.__get__('_emptyState'), {
                    fetchedContacts: 'fetched contacts'
                });
                ContactsStore.__set__('_state', initialState);

                ContactsStore.handleAction(action);
                const state = ContactsStore.__get__('_state');

                expect(processContactsStub.withArgs('finding neverland', ContactsConstants.SORT_ORDER_DONT_SORT, 'fetched contacts'));
                expect(state.fetchedContacts).toBe('fetched contacts');
                expect(state.contacts).toBe('processed contacts');

                ContactsStore.__ResetDependency__('processContacts');
            });
        });

        describe('SORT', function() {
            it('should set sortOrder and call processContacts', function () {
                const action = {
                    actionType: ContactsConstants.SORT,
                    data: 'some sort order'
                };

                const processContactsStub = sinon.stub();
                ContactsStore.__set__('processContacts', processContactsStub);
                processContactsStub
                    .returns('processed contacts');

                const initialState = Object.assign({}, ContactsStore.__get__('_emptyState'), {
                    fetchedContacts: 'fetched contacts'
                });
                ContactsStore.__set__('_state', initialState);

                ContactsStore.handleAction(action);
                const state = ContactsStore.__get__('_state');

                expect(processContactsStub.withArgs('', 'some sort order', 'fetched contacts'));
                expect(state.fetchedContacts).toBe('fetched contacts');
                expect(state.contacts).toBe('processed contacts');

                ContactsStore.__ResetDependency__('processContacts');
            });
        });
    });
});