import {EventEmitter} from 'events';
import AppDispatcher from './../dispatcher/AppDispatcher';
import ContactsConstants from './../constants/ContactsConstants';
import ContactDetailConstants from './../constants/ContactDetailConstants';

const _emptyState = {
    fetchedContacts: [],
    contacts: [],
    searchText: '',
    sortOrder: ContactsConstants.SORT_ORDER_DONT_SORT,
    status: {
        fetched: false,
        loading: false,
        error: false
    }
};

let _state = _emptyState;

/**
 * Returns new array with items that contains searchText in name property
 * @param {String} searchText
 * @param {Array} contacts
 * @returns {Array}
 */
function searchByName(searchText, contacts) {
    return contacts
        .filter(
            contact => contact.name.toLowerCase().indexOf(searchText) !== -1
        );
}

/**
 * Calls searchByName if searchText is not empty
 * @param {String} searchText
 * @param {Array} contacts
 * @returns {Array}
 */
function searchContacts(searchText, contacts) {
    return searchText && searchText.length > 0 ? searchByName(searchText, contacts) : contacts;
}

/**
 * Sorts array.
 * Ascending if order is not defined or is SORT_ORDER_ASCENDING.
 * Descending if order is SORT_ORDER_DESCENDING
 * @param {String|undefined} order
 * @param {Array} contacts
 * @returns {Array}
 */
function sortByName(order, contacts) {
    let multiplier = 1;
    if(order === ContactsConstants.SORT_ORDER_DESCENDING) {
        multiplier = -1;
    }

    return contacts.concat().sort(function(a, b) {
        const aName = a.name.toLowerCase();
        const bName = b.name.toLowerCase();

        if(aName < bName) {
            return -1 * multiplier;
        }
        else if(aName > bName) {
            return multiplier;
        }
        else {
            return 0;
        }
    });
}

/**
 * Calls sortByName if sortOrder is defined
 * @param {String|undefined} sortOrder
 * @param {Array} contacts
 * @returns {Array}
 */
function sortContacts(sortOrder, contacts) {
    return sortOrder && sortOrder !== ContactsConstants.SORT_ORDER_DONT_SORT ? sortByName(sortOrder, contacts) : contacts;
}

/**
 * First searches contacts and then filters them
 * @param {String} searchText
 * @param {String} sortOrder
 * @param {Array} contacts
 * @returns {Array}
 */
function processContacts(searchText, sortOrder, contacts) {
    const filteredContacts = searchContacts(searchText, contacts);
    const sortedContacts = sortContacts(sortOrder, filteredContacts);

    return sortedContacts;
}

var ContactsStore = Object.assign({}, EventEmitter.prototype, {
    getState: function () {
        return _state;
    },

    emitChange: function () {
        this.emit('change');
    },

    addChangeListener: function (callback) {
        this.on('change', callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener('change', callback);
    },

    handleAction: function (action) {
        switch (action.actionType) {
            case ContactsConstants.FETCH_CONTACT_LIST:
            case ContactDetailConstants.LOAD_CONTACT: {
                _state = Object.assign({}, _state, {
                    status: {
                        loading: true,
                        error: false
                    }
                });

                break;
            }
            case ContactsConstants.FETCH_CONTACT_LIST_SUCCESS: {
                _state = Object.assign({}, _state, {
                    fetchedContacts: action.data,
                    contacts: processContacts(_state.searchText, _state.sortOrder, action.data),
                    status: {
                        fetched: true,
                        loading: false,
                        error: false
                    }
                });

                break;
            }
            case ContactsConstants.FETCH_CONTACT_LIST_ERROR: {
                _state = Object.assign({}, _state, {
                    status: {
                        loading: false,
                        error: true
                    }
                });

                break;
            }
            case ContactsConstants.SEARCH: {
                const searchText = action.data;

                _state = Object.assign({}, _state, {
                    searchText: searchText,
                    contacts: processContacts(searchText, _state.sortOrder, _state.fetchedContacts)
                });

                break;
            }
            case ContactsConstants.SORT: {
                const sortOrder = action.data;

                _state = Object.assign({}, _state, {
                    sortOrder: sortOrder,
                    contacts: processContacts(_state.searchText, sortOrder, _state.fetchedContacts)
                });

                break;
            }
            default: {
                // Dont emit change event
                return;
            }
        }

        ContactsStore.emitChange();

        return;
    }
});

ContactsStore.dispatcherToken = AppDispatcher.register(ContactsStore.handleAction);

export default ContactsStore;