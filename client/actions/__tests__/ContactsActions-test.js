/* eslint-env node */
import sinon from 'sinon';
import expect from 'expect';
import ContactsActions from './../ContactsActions';
import ContactsConstants from './../../constants/ContactsConstants';

describe('ConstantsActions', function() {
    let AppDispatcher;
    let AppDispatcherMock;
    let superagent;
    let superagentMock;

    before(function () {
        AppDispatcher = ContactsActions.__get__('AppDispatcher');
        superagent = ContactsActions.__get__('superagent');

        if(!global.window) {
            global.window = {};
        }

        global.window.APP_CONFIG = {
            dataUrl: 'data url'
        };
    });

    beforeEach(function () {
        AppDispatcherMock = sinon.mock(AppDispatcher);
        superagentMock = sinon.mock(superagent);
    });
    afterEach(function () {
        AppDispatcherMock.restore();
        superagentMock.restore();
    });

    describe('#fetchContactList', function () {
        it('should dispatch FETCH_CONTACT_LIST action', function () {
            AppDispatcherMock
                .expects('handleAction')
                .withArgs({
                    actionType: ContactsConstants.FETCH_CONTACT_LIST
                });

            superagentMock
                .expects('get')
                .returns({
                    end: sinon.stub()
                });

            ContactsActions.fetchContactList();

            AppDispatcherMock.verify();

            superagentMock.restore();
        });

        it('should make request with superagent', function () {
            const endStub = sinon.stub();
            superagentMock
                .expects('get')
                .withArgs('data url')
                .returns({
                    end: endStub
                });

            ContactsActions.fetchContactList();

            superagentMock.verify();
            expect(endStub.calledOnce).toBe(true);
        });

        it('should dispatch FETCH_CONTACT_LIST_ERROR action on request error', function () {
            const endStub = sinon.stub();
            superagentMock
                .expects('get')
                .returns({
                    end: endStub
                });

            ContactsActions.fetchContactList();

            const endCallback = endStub.args[0][0];

            AppDispatcherMock
                .expects('handleAction')
                .withArgs({
                    actionType: ContactsConstants.FETCH_CONTACT_LIST_ERROR,
                    data: 'some error'
                });

            endCallback(true, {error: 'some error'});

            AppDispatcherMock.verify();
            superagentMock.restore();
        });

        it('should dispatch FETCH_CONTACT_LIST_SUCCESS action on request success', function () {
            const endStub = sinon.stub();
            superagentMock
                .expects('get')
                .returns({
                    end: endStub
                });

            ContactsActions.fetchContactList();

            const endCallback = endStub.args[0][0];

            AppDispatcherMock
                .expects('handleAction')
                .withArgs({
                    actionType: ContactsConstants.FETCH_CONTACT_LIST_SUCCESS,
                    data: 'request response'
                });

            endCallback(false, {body: 'request response'});

            AppDispatcherMock.verify();
            superagentMock.restore();
        });
    });

    describe('#search', function () {
        it('should dispatch SEARCH action', function () {
            AppDispatcherMock
                .expects('handleAction')
                .withArgs({
                    actionType: ContactsConstants.SEARCH,
                    data: 'search text'
                });

            ContactsActions.search('search text');

            AppDispatcherMock.verify();
        });
    });

    describe('#sortAscending', function () {
        it('should dispatch SORT action with SORT_ORDER_ASCENDING param', function () {
            AppDispatcherMock
                .expects('handleAction')
                .withArgs({
                    actionType: ContactsConstants.SORT,
                    data: ContactsConstants.SORT_ORDER_ASCENDING
                });

            ContactsActions.sortAscending();

            AppDispatcherMock.verify();
        });
    });

    describe('#sortDescending', function () {
        it('should dispatch SORT action with SORT_ORDER_DESCENDING param', function () {
            AppDispatcherMock
                .expects('handleAction')
                .withArgs({
                    actionType: ContactsConstants.SORT,
                    data: ContactsConstants.SORT_ORDER_DESCENDING
                });

            ContactsActions.sortDescending();

            AppDispatcherMock.verify();
        });
    });

    describe('#dontSort', function () {
        it('should dispatch SORT action with SORT_ORDER_DONT_SORT param', function () {
            AppDispatcherMock
                .expects('handleAction')
                .withArgs({
                    actionType: ContactsConstants.SORT,
                    data: ContactsConstants.SORT_ORDER_DONT_SORT
                });

            ContactsActions.dontSort();

            AppDispatcherMock.verify();
        });
    });
});