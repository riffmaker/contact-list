import superagent from 'superagent';
import AppDispatcher from './../dispatcher/AppDispatcher';
import ContactsConstants from './../constants/ContactsConstants';



export default {
    fetchContactList: function(){
        AppDispatcher.handleAction({
            actionType: ContactsConstants.FETCH_CONTACT_LIST
        });
        
        superagent
            .get(window.APP_CONFIG.dataUrl)
            .end((error, response) => {
                if(error) {
                    AppDispatcher.handleAction({
                        actionType: ContactsConstants.FETCH_CONTACT_LIST_ERROR,
                        data: response.error
                    });
                }
                else {
                    AppDispatcher.handleAction({
                        actionType: ContactsConstants.FETCH_CONTACT_LIST_SUCCESS,
                        data: response.body
                    });
                }

            });
    },

    search: function(searchText) {
        AppDispatcher.handleAction({
            actionType: ContactsConstants.SEARCH,
            data: searchText
        });
    },

    sortAscending: function() {
        AppDispatcher.handleAction({
            actionType: ContactsConstants.SORT,
            data: ContactsConstants.SORT_ORDER_ASCENDING
        });
    },

    sortDescending: function() {
        AppDispatcher.handleAction({
            actionType: ContactsConstants.SORT,
            data: ContactsConstants.SORT_ORDER_DESCENDING
        });
    },

    dontSort: function() {
        AppDispatcher.handleAction({
            actionType: ContactsConstants.SORT,
            data: ContactsConstants.SORT_ORDER_DONT_SORT
        });
    }
}
