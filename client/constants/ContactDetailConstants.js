export default {
    LOAD_CONTACT: 'CONTACT_DETAIL.LOAD_CONTACT',
    LOAD_CONTACT_SUCCESS: 'CONTACT_DETAIL.LOAD_CONTACT_SUCCESS',
    LOAD_CONTACT_ERROR: 'CONTACT_DETAIL.LOAD_CONTACT_ERROR'
}
