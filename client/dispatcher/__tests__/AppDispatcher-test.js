import sinon from 'sinon';
import AppDispatcher from '../AppDispatcher';

describe('AppDispatcher', function(){
    it('calls dispatch', function(){
        const AppDispatcherMock = sinon.mock(AppDispatcher);

        const action = {
            actionType: 'test',
            data: 'test data'
        };

        AppDispatcherMock
            .expects('dispatch')
            .withArgs(action)
            .once();

        AppDispatcher.handleAction(action);

        AppDispatcherMock.verify();
    });


});
