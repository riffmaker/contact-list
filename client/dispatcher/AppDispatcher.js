import {Dispatcher} from 'flux';

let AppDispatcher = new Dispatcher();

AppDispatcher.handleAction = function(action){
    this.dispatch(action);
};

export default AppDispatcher;