import React from 'react';
import {Route, IndexRoute} from 'react-router';
import ContactList from './components/ContactList/ContactList';
import ContactDetail from './components/ContactDetail/ContactDetail';
import Admin from './components/Admin/Admin';

export default (
    <Route path="/">
        <IndexRoute component={ContactList} />
        <Route path="contact/:id" component={ContactDetail} />
        <Route path="admin" component={Admin} />
    </Route>
);
