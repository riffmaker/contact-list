import React from 'react';
import styles from './loadingSpinner.css';
import spinnerImageUrl from './spinner.gif';

function LoadingSpinner({isLoading, children, className}) {
    const content = isLoading ? <img src={spinnerImageUrl} /> : children;

    return (
        <div className={isLoading === true ? styles.containerLoading + ' ' + className : className}>
            {content}
        </div>
    );
}

LoadingSpinner.propTypes = {
    isLoading: React.PropTypes.bool.isRequired,
    children: React.PropTypes.any,
    className: React.PropTypes.string
};

export default LoadingSpinner;