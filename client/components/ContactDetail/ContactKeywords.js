import React from 'react';
import styles from './ContactKeywords.css';

function ContactKeywords({keywords}) {
    return (
        <div className={styles.keywords}>
            {
                keywords.map(
                    (keyword, index) => <div key={index} className={styles.keyword}>{ContactKeywords.capitalizeFirstLetter(keyword)}</div>
                )
            }
        </div>
    );
}

ContactKeywords.capitalizeFirstLetter = function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

ContactKeywords.propTypes = {
    keywords: React.PropTypes.arrayOf(React.PropTypes.string).isRequired
};

export default ContactKeywords;

