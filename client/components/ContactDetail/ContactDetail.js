import React from 'react';
import {Link} from 'react-router';
import ContactsActions from './../../actions/ContactsActions';
import ContactsStore from './../../stores/ContactsStore';
import LoadingSpinner from './../LoadingSpinner/LoadingSpinner';
import ContactCard from './ContactCard';
import styles from './ContactDetail.css';

class ContactDetail extends React.Component {

    constructor(props) {
        super(props);

        this.state = ContactsStore.getState();

        this._onChange= this._onChange.bind(this);
    }

    componentDidMount() {
        ContactsStore.addChangeListener(this._onChange);

        if(this.state.status.fetched === false && this.state.status.loading === false) {
            ContactsActions.fetchContactList();
        }
    }

    componentWillUnmount() {
        ContactsStore.removeChangeListener(this._onChange);
    }

    static getContact(id, contacts) {
        for(let i = 0, length = contacts.length; i < length; i += 1) {
            const contact = contacts[i];

            if(contact.id === id) {
                return contact;
            }
        }

        return null;
    }

    _onChange() {
        this.setState(ContactsStore.getState());
    }

    render() {
        let content;
        if(this.state.status.error === true) {
            content = 'Could not load contact';
        }
        else if(this.state.status.fetched === true) {
            const contact = ContactDetail.getContact(parseInt(this.props.params.id, 10), this.state.contacts);

            if(contact === null ) {
                content = 'Contact was not found.';
            }
            else {
                content = <ContactCard contact={contact} />;
            }
        }

        return (
            <div>
                <div className={styles.topBarContainer}>
                    <Link to="/">Back to contact list</Link>
                </div>

                <LoadingSpinner className={styles.container} isLoading={this.state.status.loading}>
                    {content}
                </LoadingSpinner>
            </div>
        );
    }
}

ContactDetail.propTypes = {
    params: React.PropTypes.shape({
        id: React.PropTypes.string.isRequired
    }).isRequired
};

export default ContactDetail;