import sinon from 'sinon';
import ContactDetail from './../ContactDetail';

describe('ContactDetail', function() {
    let ContactsStore;
    let ContactsStoreMock;
    let ContactsActions;
    let ContactsActionsMock;

    before(function() {
        ContactsStore = ContactDetail.__get__('ContactsStore');
        ContactsActions = ContactDetail.__get__('ContactsActions');
    });

    beforeEach(function() {
        ContactsStoreMock = sinon.mock(ContactsStore);
        ContactsActionsMock = sinon.mock(ContactsActions);
    });
    afterEach(function() {
        ContactsStoreMock.restore();
        ContactsActionsMock.restore();
    });

    describe('#componentDidMount', function() {
        it('should add change listener to ContactsStore and call fetchContactDetail action if data is not fetched', function() {
            ContactsStoreMock
                .expects('getState')
                .returns({
                    status: {
                        fetched: false,
                        loading: false,
                        error: false
                    }
                });

            const instance = new ContactDetail();

            ContactsStoreMock
                .expects('addChangeListener')
                .withArgs(instance._onChange)
                .once();

            ContactsActionsMock
                .expects('fetchContactList')
                .once();

            instance.componentDidMount();

            ContactsStoreMock.verify();
            ContactsActionsMock.verify();
        });

        it('should add change listener to ContactsStore and should not call fetchContactDetail action if data is fetched', function() {
            ContactsStoreMock
                .expects('getState')
                .returns({
                    status: {
                        fetched: true,
                        loading: false,
                        error: false
                    }
                });

            const instance = new ContactDetail();

            ContactsStoreMock
                .expects('addChangeListener')
                .withArgs(instance._onChange)
                .once();

            ContactsActionsMock
                .expects('fetchContactList')
                .never();

            instance.componentDidMount();

            ContactsStoreMock.verify();
            ContactsActionsMock.verify();
        });
    });

    describe('#componentWillUnmount', function() {
        it('should remove change listener from ContactsStore and reset search', function() {
            const instance = new ContactDetail();

            ContactsStoreMock
                .expects('removeChangeListener')
                .withArgs(instance._onChange)
                .once();

            instance.componentWillUnmount();

            ContactsStoreMock.verify();
        });
    });

    describe('#_onChange', function() {
        it('should get state from ContactsStore and set it to own state', function() {
            const instance = new ContactDetail();

            ContactsStoreMock
                .expects('getState')
                .returns('some state')
                .once();

            const ContactDetailMock = sinon.mock(instance);
            ContactDetailMock
                .expects('setState')
                .withArgs('some state')
                .once();

            instance._onChange();

            ContactsStoreMock.verify();
            ContactDetailMock.verify();
        });
    });
});