import React from 'react';
import styles from './ContactName.css';

function ContactName({name}) {
    const names = name.split(" ");
    const surname = names.pop();

    return (
        <span>
            {names.map((name,index) => <span key={index}>{name} </span>)}
            <span className={styles.surname}>{surname}</span>
        </span>
    )
}

ContactName.propTypes = {
    name: React.PropTypes.string.isRequired
};

export default ContactName;