import React from 'react';
import ContactKeywords from './ContactKeywords';
import ContactName from './ContactName';
import styles from './ContactCard.css';

function ContactCard({contact}) {
    return (
        <div className={styles.container}>
            <div className={styles.identity}>
                <h1 className={styles.name}><ContactName name={contact.name} /></h1>
                <h3 className={styles.company}>{contact.company.name}</h3>
            </div>

            <div className={styles.info}>
                <ContactKeywords keywords={contact.company.bs.split(' ')} />

                <div>
                    <strong>Phone:</strong> {contact.phone}
                </div>
                <div>
                    <strong>E-Mail:</strong> {contact.email}
                </div>
                <div>
                    <strong>URL:</strong> {contact.website}
                </div>
            </div>
        </div>
    );
}

ContactCard.propTypes = {
    contact: React.PropTypes.shape({
        name: React.PropTypes.string.isRequired,
        company: React.PropTypes.shape({
            name: React.PropTypes.string.isRequired,
            bs: React.PropTypes.string.isRequired
        }).isRequired,
        phone: React.PropTypes.string.isRequired,
        email: React.PropTypes.string.isRequired,
        website: React.PropTypes.string.isRequired
    }).isRequired
};

export default ContactCard;