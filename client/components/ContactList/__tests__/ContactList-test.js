import React from 'react';
import expect from 'expect';
import sinon from 'sinon';
import {shallow} from 'enzyme';
import ContactList from './../ContactList';

describe('ContactList', function() {
    let ContactsStore;
    let ContactsStoreMock;
    let ContactsActions;
    let ContactsActionsMock;

    before(function() {
        ContactsStore = ContactList.__get__('ContactsStore');
        ContactsActions = ContactList.__get__('ContactsActions');
    });

    beforeEach(function() {
        ContactsStoreMock = sinon.mock(ContactsStore);
        ContactsActionsMock = sinon.mock(ContactsActions);
    });
    afterEach(function() {
        ContactsStoreMock.restore();
        ContactsActionsMock.restore();
    });

    describe('#componentDidMount', function() {
        it('should add change listener to ContactsStore and call fetchContactList action', function() {
            const instance = new ContactList();

            ContactsStoreMock
                .expects('addChangeListener')
                .withArgs(instance._onChange)
                .once();

            ContactsActionsMock
                .expects('fetchContactList')
                .once();

            instance.componentDidMount();

            ContactsStoreMock.verify();
            ContactsActionsMock.verify();
        });
    });

    describe('#componentWillUnmount', function() {
        it('should remove change listener from ContactsStore and reset search', function() {
            const instance = new ContactList();

            ContactsStoreMock
                .expects('removeChangeListener')
                .withArgs(instance._onChange)
                .once();

            ContactsActionsMock
                .expects('search')
                .withArgs('')
                .once();

            instance.componentWillUnmount();

            ContactsStoreMock.verify();
            ContactsActionsMock.verify();
        });
    });

    describe('#_onChange', function() {
        it('should get state from ContactsStore and set it to own state', function() {
            const instance = new ContactList();

            ContactsStoreMock
                .expects('getState')
                .returns('some state')
                .once();

            const ContactListMock = sinon.mock(instance);
            ContactListMock
                .expects('setState')
                .withArgs('some state')
                .once();

            instance._onChange();

            ContactsStoreMock.verify();
            ContactListMock.verify();
        });
    });

    describe('#render', function() {
         it('should render ContactListItems',  function() {
             const contacts = [
                 {
                     id: 1,
                     name: 'Bart'
                 },
                 {
                     id: 2,
                     name: 'Lisa'
                 },
                 {
                     id: 3,
                     name: 'Homer'
                 }
             ];

             ContactsStoreMock
                 .expects('getState')
                 .returns({
                     contacts: contacts,
                     status: {
                         fetched: true,
                         loading: false,
                         error: false
                     }
                 });

             ContactsActionsMock
                 .expects('fetchContactList');

             const contactList = shallow(<ContactList />);
             const contactListItems = contactList.find('ContactListItem');
             expect(contactListItems.length).toBe(3);
             expect(contactListItems.at(0).prop('id')).toBe(1);
             expect(contactListItems.at(0).prop('name')).toBe('Bart');
             expect(contactListItems.at(1).prop('id')).toBe(2);
             expect(contactListItems.at(1).prop('name')).toBe('Lisa');
             expect(contactListItems.at(2).prop('id')).toBe(3);
             expect(contactListItems.at(2).prop('name')).toBe('Homer');

             ContactsStoreMock.restore();
             ContactsActionsMock.restore();
         });
    });
});