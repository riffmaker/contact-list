import React from 'react';
import {Link} from 'react-router';
import ContactsActions from './../../actions/ContactsActions';
import ContactsStore from './../../stores/ContactsStore';
import LoadingSpinner from './../LoadingSpinner/LoadingSpinner';
import ContactListItem from './ContactListItem';
import styles from './ContactList.css';

export default class ContactList extends React.Component {
    constructor(props) {
        super(props);

        this.state = ContactsStore.getState();

        this._onChange= this._onChange.bind(this);
        this.onSearchChange = this.onSearchChange.bind(this);
    }
    
    componentDidMount() {
        ContactsStore.addChangeListener(this._onChange);
        ContactsActions.fetchContactList();
    }

    componentWillUnmount() {
        ContactsStore.removeChangeListener(this._onChange);
        ContactsActions.search('');
    }

    _onChange() {
        this.setState(ContactsStore.getState());
    }

    onSearchChange(e) {
        const searchText = e.target.value;
        
        if(this.state.searchTimerId) {
            clearTimeout(this.state.searchTimerId)
        }
        
        const searchTimerId = setTimeout(() => {ContactsActions.search(searchText)}, 300);
        
        this.setState({
            searchTimerId: searchTimerId
        });
    }

    sortAscending() {
        ContactsActions.sortAscending();
    }

    sortDescending() {
        ContactsActions.sortDescending();
    }

    dontSort() {
        ContactsActions.dontSort();
    }

    render() {
        let content;

        if(this.state.status.loading === false) {
            if(this.state.status.error === true) {
                content = 'Could not load contacts';
            }
            else {
                const contactList = this.state.contacts.map((contact, index) => <ContactListItem key={index} id={contact.id} name={contact.name} />);

                content = (
                    <div>
                        <div className={styles.filter}>
                            <input type="text" className={styles.searchBar} placeholder="Search" value={this.state.searchText} onChange={this.onSearchChange} />

                            <span className={styles.sorting}>
                                <span className={styles.sortLabel}>Sort order:</span>
                                <button onClick={this.sortAscending}>Ascending</button>
                                <button onClick={this.sortDescending}>Descending</button>
                                <button onClick={this.dontSort}>None</button>
                            </span>
                        </div>

                        {contactList.length > 0 ? contactList : 'No contacts found.'}
                    </div>

                );
            }
        }

        return (
            <div className={styles.container}>
                <div className="header">
                    <h1>Contact list</h1>
                    <Link className="button" to="/admin">Admin</Link>
                </div>

                <LoadingSpinner isLoading={this.state.status.loading}>
                    {content}
                </LoadingSpinner>
            </div>
        );
    }
}