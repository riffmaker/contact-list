import React from 'react';
import {Link} from 'react-router';
import styles from './ContactListItem.css';

function ContactListItem({id, name}) {
    return (
        <div>
            <Link className={styles.link} to={`contact/${id}`}>
                {name}
            </Link>
        </div>
    );
}

ContactListItem.propTypes = {
    id: React.PropTypes.number.isRequired,
    name: React.PropTypes.string.isRequired
};

export default ContactListItem;