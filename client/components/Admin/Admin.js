import React from 'react';
import {Link} from 'react-router';
import ContactsActions from './../../actions/ContactsActions';
import ContactsStore from './../../stores/ContactsStore';
import LoadingSpinner from './../LoadingSpinner/LoadingSpinner';
import styles from './Admin.css';

export default class Admin extends React.Component {
    constructor(props) {
        super(props);

        this.state = ContactsStore.getState();

        this._onChange= this._onChange.bind(this);
    }
    
    componentDidMount() {
        ContactsStore.addChangeListener(this._onChange);
        ContactsActions.fetchContactList();
    }

    componentWillUnmount() {
        ContactsStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(ContactsStore.getState());
    }

    static getLetterCounts(contacts) {
        const counts = {};
        
        for(let i = 0, length = contacts.length; i < length; i += 1) {
            const contact = contacts[i];
            const letter = contact.name.charAt(0).toLowerCase();
            
            if(counts[letter]) {
                counts[letter] += 1
            }
            else {
                counts[letter] = 1
            }
        }
        
        return counts;
    }

    static getLetterCountsList(letterCounts) {
        const letterCountsList = [];

        for(let letter in letterCounts) {
            if(letterCounts.hasOwnProperty(letter)) {
                const count = letterCounts[letter];

                letterCountsList.push(
                    <div key={letter}>
                        <span className={styles.letterCountLabel}>{letter.toUpperCase()}:</span> {count}
                    </div>
                );
            }
        }

        return letterCountsList;
    }
    
    render() {
        let content;
        
        if(this.state.status.loading === false) {
            if(this.state.status.error === true) {
                content = 'Could not load contacts';
            }
            else {
                const letterCounts = Admin.getLetterCounts(this.state.contacts);
                
                content = (
                    <div>
                        <h3>First letter counts:</h3>
                        
                        {Admin.getLetterCountsList(letterCounts)}
                    </div>
                );
            }
        }

        return (
            <div className={styles.container}>
                <div className="header">
                    <h1>Contact admin</h1>
                    <Link className="button" to="/">Contact list</Link>
                </div>


                <LoadingSpinner isLoading={this.state.status.loading}>
                    {content}
                </LoadingSpinner>
            </div>
        );
    }
}