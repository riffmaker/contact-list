/* eslint-env node */
require('babel-core/register');
var gulp = require('gulp');
var eslint = require('gulp-eslint');
var gutil = require('gulp-util');
var webpack = require('webpack');
var istanbul = require('gulp-istanbul');
var isparta = require('isparta');
var mocha = require('gulp-mocha');
var minimist = require('minimist');
var PrettyError = require('pretty-error');
require('ignore-styles');

var pe = new PrettyError();

function renderError(error) {
    error = pe.render(error);
    return error;
}

var options = minimist(process.argv.slice(2));

gulp.task('webpack', function(){
    var config = require('./webpack.config.js');
    var compiler = webpack(config);

    compiler.run(function(err){
        if(err) {
            gutil.log('Error: ', err);
        }
        else {
            gutil.log('Build complete.')
        }
    });
});

gulp.task('lint', function(){
    return gulp.src([
            '**/*.js',
            '!./**/node_modules/**',
            '!./public/**'
        ])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError())
        .on('error', function(error){
            gutil.log(renderError(error));
            this.emit('end');
        });
});

var coverageVariable;
gulp.task('test-coverage', function(){
    // Change coverageVariable each time task is run to prevent endless loop
    coverageVariable = '$$cov_' + new Date().getTime() + '$$';

    return gulp.src([
            './client/**/*.js',
            '!./client/**/*-test.js',
        ])
        //.pipe(debug())
        // Covering files
        .pipe(istanbul({
            instrumenter: isparta.Instrumenter,
            includeUntested: true,
            coverageVariable: coverageVariable
        }))
        // Force `require` to return covered files
        .pipe(istanbul.hookRequire());
});

gulp.task('mocha', ['test-coverage'], function(){
    return gulp.src([
            './client/**/*-test.js'
        ], {read:false})
        .pipe(mocha({
            reporter: 'spec',
            timeout: '5000',
            // require: [ignoreStyles]
        }))
        .on('error', function(error){
            gutil.log(renderError(error));
            this.emit('end');
        })
        .pipe(istanbul.writeReports({
            dir: './public/coverage',
            reporters: ['html', 'text-summary'],
            coverageVariable: coverageVariable
        }))

        // Enforce a coverage of at least 90%
        .pipe(istanbul.enforceThresholds({ thresholds: { global: 90 } }))
        .on('error', function(error){
            gutil.log(renderError(error));
            this.emit('end');
        });
});

gulp.task('test', ['lint', 'mocha']);
gulp.task('testwatch', ['lint', 'mocha'], function(){
    return gulp.watch([
        'client/**/*.js'
    ], ['lint', 'mocha' ]);
});

gulp.task('testsingle', function(){
    const fileName = './client/**/' + options.file + '-test.js';
    gutil.log('Testing file: ', fileName);
    setTimeout(function(){
        return gulp.src([
                fileName
            ], {read:false})
            .pipe(mocha({
                timeout: '5000'
            }))
            .on('error', function(error){
                gutil.log(renderError(error));
                this.emit('end');
            })
    }, 500);
});

gulp.task('testsinglewatch', ['testsingle'], function(){
    return gulp.watch([
        './client/**/' + options.file + '-test.js',
        './client/**/' + options.file + '.js'
    ], ['testsingle']);
});

gulp.task('default', ['webpack']);