/* eslint-env node */
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var ProgressBarPlugin = require('progress-bar-webpack-plugin');

const config = {
    entry: [
        path.join(__dirname,'/client/app.js')
    ],
    output: {
        path: path.resolve("./public/build"),
        filename: 'js/bundle.js',
        publicPath: '/'
    },
    module: {
        loaders: [
            {
                test: /\.(jsx|js)$/,
                exclude: /node_modules/,
                loaders: ['babel'],
                include: path.join(__dirname, 'client')
            },
            { test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader?modules=local" + (process.env.NODE_ENV === 'production' ? "&minimize" : "-minimize&localIdentName=[name]--[local]--[hash:base64:5]")) },
            { test: /\.gif$/, loader: "file-loader?&name=./images/[name]-[hash].[ext]" },
            { test: /\.jpg$/, loader: "file-loader?&name=./images/[name]-[hash].[ext]" }
        ]
    },
    plugins: [
        new ExtractTextPlugin('css/style.css', { allChunks: true }),
        new ProgressBarPlugin({
            format: '  build [:bar] :percent (:elapsed seconds)',
            clear: true,
            width: 60
        })
    ],
    devtool: 'source-map',
    debug: true
};

if(process.env.NODE_ENV === 'production') {
    config.devtool = false;
    config.debug = false;
    config.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false }
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    );
}

module.exports = config;
